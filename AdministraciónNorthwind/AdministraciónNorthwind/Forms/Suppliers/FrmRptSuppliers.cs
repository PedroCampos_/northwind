﻿using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdministraciónNorthwind.Forms.Suppliers
{
    public partial class FrmRptSuppliers : Form
    {
        ParameterFields parametros = new ParameterFields();
        ParameterField miParametro = new ParameterField();
        ParameterDiscreteValue valor = new ParameterDiscreteValue();
        DAO.DaoSuppliers daosupp = new DAO.DaoSuppliers();
        public FrmRptSuppliers()
        {
            InitializeComponent();
            cargarCombo();
        }

        public void cargarCombo()
        {
            cmbCountry.DataSource = daosupp.listsuppliers().Select(x => x.Country).Distinct().ToList();
        }

       

        private void btnBuscar_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.miParametro.ParameterValueType = ParameterValueKind.StringParameter;
                this.miParametro.Name = "@pais";
                this.valor.Value = cmbCountry.Text;
                this.miParametro.CurrentValues.Add(valor);
                this.parametros.Add(miParametro);

                this.crystalReportViewer1.ParameterFieldInfo = parametros;
                Reportes.rptProveedores rpt = new Reportes.rptProveedores();
                this.crystalReportViewer1.ReportSource = rpt;
            }
            catch (Exception)
            {

            }
        }
    }
}

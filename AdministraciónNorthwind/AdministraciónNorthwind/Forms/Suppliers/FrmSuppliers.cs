﻿using AdministraciónNorthwind.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdministraciónNorthwind.Forms.Suppliers
{
    public partial class FrmSuppliers : Form
    {

        bool agregar = false;
        bool modificar = false;
        DaoSuppliers daosupp = new DaoSuppliers();
        private string[] columns = new[] {"Supplier ID", "Company Name","Contact Name","Contact Title","Address","City",
            "Region","Postal Code","Country","Phone","Fax","Home Page"};

        public FrmSuppliers()
        {
            InitializeComponent();
            cargardatos();

        }


        private void cargardatos()
        {
            List<Model.Suppliers> suppliers = new List<Model.Suppliers>();
            try
            {
                suppliers = daosupp.listsuppliers();
                dgvdatos.Columns.Clear();
                dgvdatos.DataSource = suppliers.Select(x => new

                {
                    x.SupplierID,
                    x.CompanyName,
                    x.ContactName,
                    x.ContactTitle,
                    x.Address,
                    x.City,
                    x.Region,
                    x.PostalCode,
                    x.Country,
                    x.Phone,
                    x.Fax,
                    x.HomePage,


                }).ToList();

                for (int i=0; i< columns.Count(); i++)
                {
                    dgvdatos.Columns[i].HeaderText = columns[i];
                }

                if (dgvdatos.RowCount > 0)
                {
                    dgvdatos.Rows[0].Selected = false;
                }

            }catch(Exception ex)
            {
                MessageBox.Show("Ocurrió un error" + ex.Message);
            }
        }


        private void llenar()
        {
            if (dgvdatos.RowCount > 0)
            {
                txtCompany.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[1].Value.ToString();
                txtContactName.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[2].Value.ToString();
                txtContactTitle.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[3].Value.ToString();
                txtAddress.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[4].Value.ToString();
                txtCity.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[5].Value.ToString();
                if (dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[6].Value != null)
                {
                    txtRegion.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[6].Value.ToString();
                }
                
                txtPostal.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[7].Value.ToString();
                txtCountry.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[8].Value.ToString();
                txtPhone.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[9].Value.ToString();
                if (dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[10].Value != null)
                {
                    txtFax.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[10].Value.ToString();
                }
                if (dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[11].Value!=null)
                {
                    txtHomepage.Text = dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells[11].Value.ToString();
                }

            }
        }



        private void habilitarcampos()
        {
            txtCompany.Enabled = true;
            txtContactName.Enabled = true;
            txtContactTitle.Enabled = true;
            txtAddress.Enabled = true;
            txtCity.Enabled = true;
            txtRegion.Enabled = true;
            txtPostal.Enabled = true;
            txtCountry.Enabled = true;
            txtPhone.Enabled = true;
            txtFax.Enabled = true;
            txtHomepage.Enabled = true;
        }

        private void deshabilitarcampos()
        {
            txtCompany.Enabled = false;
            txtContactName.Enabled = false ;
            txtContactTitle.Enabled = false;
            txtAddress.Enabled = false;
            txtCity.Enabled = false;
            txtRegion.Enabled = false;
            txtPostal.Enabled = false;
            txtCountry.Enabled = false;
            txtPhone.Enabled = false;
            txtFax.Enabled = false;
            txtHomepage.Enabled = false;
        }

        private Model.Suppliers setear()
        {
            try{
                Model.Suppliers supp = new Model.Suppliers();
                supp.CompanyName = txtCompany.Text;
                supp.ContactName = txtContactName.Text;
                supp.ContactTitle = txtContactTitle.Text;
                supp.Address = txtAddress.Text;
                supp.City = txtCity.Text;
                supp.Region = txtRegion.Text;
                supp.PostalCode = txtPostal.Text;
                supp.Country = txtCountry.Text;
                supp.Phone = txtPhone.Text;
                supp.Fax = txtFax.Text;
                supp.HomePage = txtHomepage.Text;

                return supp;

            }catch (Exception ex)
            {
                return new Model.Suppliers();
            }
        }


        private bool validadcampos()
        {
            bool estado = false;
            if (!txtCompany.Text.Equals("") && (!txtContactName.Text.Equals(""))
                && (!txtContactTitle.Text.Equals("")) && (!txtAddress.Text.Equals("")) && (!txtCity.Text.Equals(""))
                && (!txtRegion.Text.Equals("")) && (!txtPostal.Text.Equals("")) && (!txtCountry.Text.Equals(""))
                && (!txtPhone.Text.Equals("")) && (!txtFax.Text.Equals("")) && (!txtHomepage.Text.Equals("")))
            {
                estado = true;
            }
            else
            {
                estado = false;
            }

            return estado;
        }


        private void limpiar()
        {
            txtCompany.Clear();
            txtContactName.Clear();
            txtContactTitle.Clear();
            txtAddress.Clear();
            txtCity.Clear();
            txtRegion.Clear();
            txtPostal.Clear();
            txtCountry.Clear();
            txtPhone.Clear();
            txtFax.Clear();
            txtHomepage.Clear();
        }


        private void estadoBTN(string texto)
        {
            if (texto == "Agregar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
            }
            else if (texto == "Modificar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
            }

            else
            {
                btnAgregar.Enabled = true;
                btnGuardar.Enabled = false;
                btnEliminar.Enabled = true;
                btnModificar.Enabled = true;

            }
        }

        private void dgvdatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            llenar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            agregar = true;
            modificar = false;
            estadoBTN("Agregar");
            limpiar();
            habilitarcampos();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (validadcampos())
            {
                if (agregar == true)
                {
                    if (daosupp.agregar(setear()))
                        MessageBox.Show("Informacion registrada correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Ha ocurrido un error al guardar", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;
                }
                else if (modificar==true)
                {
                    int id = int.Parse(dgvdatos.SelectedRows[0].Cells[0].Value.ToString());
                    if (daosupp.modify(setear(), id))
                        MessageBox.Show("Informacion modificada con exito", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Ha ocurrido un error al modificar", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;



                }
                limpiar();
                cargardatos();
                deshabilitarcampos();
                estadoBTN("Nuevo");

                
            }
            else
            {
                MessageBox.Show("Los campos son requeridos", "informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (dgvdatos.SelectedRows.Count > 0)
            {
                agregar = false;
                modificar = true;
                habilitarcampos();
                estadoBTN("Modificar");
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvdatos.SelectedRows.Count > 0)
            {
                DialogResult dialog = MessageBox.Show("¿Seguro de eliminar este registro?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    int id = int.Parse(dgvdatos.SelectedRows[0].Cells[0].Value.ToString());
                    if (daosupp.eliminar(id))
                        MessageBox.Show("Registro eliminado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al tratar de eliminar registro", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("Accion cancelada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                limpiar();
                cargardatos();
            }
            else
                MessageBox.Show("Debe seleccionar un registro", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            deshabilitarcampos();
            cargardatos();
            estadoBTN("Nuevo");
        }

        private void txtCompany_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtContactName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtContactTitle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtCountry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtCity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtRegion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void FrmSuppliers_Load(object sender, EventArgs e)
        {
            estadoBTN("Nuevo");
            deshabilitarcampos();
            limpiar();
            if (dgvdatos.RowCount > 0)
            {
                dgvdatos.Rows[0].Selected = false;
            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
           if (char.IsLetter(e.KeyChar))
            {
                e.Handled = true;

            }
        }

        private void txtFax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = true;

            }
        }
    }
}

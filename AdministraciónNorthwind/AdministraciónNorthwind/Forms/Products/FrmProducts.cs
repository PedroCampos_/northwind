﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdministraciónNorthwind.DAO;
using AdministraciónNorthwind.Model;

namespace AdministraciónNorthwind.Forms.Products
{
    public partial class FrmProducts : Form
    {
        private string[] Columns = new[] { "ProductID", "ProductName", "SupplierID", "CategoryID", "QuantityPerUnit",
            "UnitPrice", "UnitsInStock", "UnitsOnOrder", "ReorderLevel", "Discontinued", };

        DaoProducts daoProducts = new DaoProducts();
        NorthwindEntities model = new NorthwindEntities();
        Model.Products products = new Model.Products();

        public FrmProducts()
        {
            InitializeComponent();
            cargarDatos();
            cargarSuppliers();
            cargarCategories();
            deshabilitar();
            deshabilitarBotones();
            cargarDiscontinued();
        }

        public void cargarDiscontinued()
        {
            cmbDiscontinued.Items.Add("True");
            cmbDiscontinued.Items.Add("False");
        }

        public void limpiar()
        {
            string flag = "";
            txtProductID.Text = flag;
            txtProductName.Text = flag;
            cmbCategoryID.SelectedIndex = 0;
            cmbSupplierID.SelectedIndex = 0;
            txtQuantityPerUnit.Text = flag;
            txtUnitPrice.Text = flag;
            txtUnitsInStock.Text = flag;
            txtUnitsOnOrder.Text = flag;
            txtReorderLevel.Text = flag;
            cmbDiscontinued.SelectedIndex = 0;
        }

        public void deshabilitar()
        {
            bool flag = false;
            txtProductID.Enabled = flag;
            txtProductName.Enabled = flag;
            cmbCategoryID.Enabled = flag;
            cmbSupplierID.Enabled = flag;
            txtQuantityPerUnit.Enabled = flag;
            txtUnitPrice.Enabled = flag;
            txtUnitsInStock.Enabled = flag;
            txtUnitsOnOrder.Enabled = flag;
            txtReorderLevel.Enabled = flag;
            cmbDiscontinued.Enabled = flag;

            btnGuardar.Enabled = flag;
            btnModificar.Enabled = flag;
            btnEliminar.Enabled = flag;
            btnCancelar.Enabled = flag;
        }

        public void habilitar()
        {
            bool flag = true;
            txtProductID.Enabled = flag;
            txtProductName.Enabled = flag;
            cmbCategoryID.Enabled = flag;
            cmbSupplierID.Enabled = flag;
            txtQuantityPerUnit.Enabled = flag;
            txtUnitPrice.Enabled = flag;
            txtUnitsInStock.Enabled = flag;
            txtUnitsOnOrder.Enabled = flag;
            txtReorderLevel.Enabled = flag;
            cmbDiscontinued.Enabled = flag;

            btnGuardar.Enabled = flag;
            btnCancelar.Enabled = flag;
        }

        public void deshabilitarBotones()
        {
            bool flag = false;

            btnModificar.Enabled = flag;
            btnEliminar.Enabled = flag;
            btnCancelar.Enabled = flag;
        }

        public void habilitarBotones()
        {
            bool flag = true;

            btnModificar.Enabled = flag;
            btnEliminar.Enabled = flag;
            btnCancelar.Enabled = flag;
        }

        public void cargarSuppliers()
        {
            cmbSupplierID.DataSource = model.Suppliers.ToList();
            cmbSupplierID.DisplayMember = "CompanyName";
            cmbSupplierID.ValueMember = "SupplierID";
        }

        public void cargarCategories()
        {
            cmbCategoryID.DataSource = model.Categories.ToList();
            cmbCategoryID.DisplayMember = "CategoryName";
            cmbCategoryID.ValueMember = "CategoryID";
        }

        public void cargarDatos()
        {
            List<Model.Products> products = new List<Model.Products>();
            try
            {
                //Recupera una lista de la base de datos de todos los products
                products = daoProducts.GetProducts();
                //limpiar valores del DataGridView
                dgvDatos.Columns.Clear();
                //Asignacion de valores al DataGridView
                dgvDatos.DataSource = products.Select(x => new
                {
                    x.ProductID,
                    x.ProductName,
                    x.SupplierID,
                    x.CategoryID,
                    x.QuantityPerUnit,
                    x.UnitPrice,
                    x.UnitsInStock,
                    x.UnitsOnOrder,
                    x.ReorderLevel,
                    x.Discontinued
                }).ToList();

                for (int i = 0; i < Columns.Count(); i++)
                    dgvDatos.Columns[i].HeaderText = Columns[i];

                if (dgvDatos.RowCount > 0)
                    dgvDatos.Rows[0].Selected = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente" + ex.Message);
            }
        }

        private void setear()
        {
            products.ProductName = txtProductName.Text;
            products.SupplierID = Convert.ToInt32(cmbSupplierID.SelectedValue.ToString());
            products.CategoryID = Convert.ToInt32(cmbCategoryID.SelectedValue.ToString());
            products.QuantityPerUnit = txtQuantityPerUnit.Text;
            products.UnitPrice = decimal.Parse(txtUnitPrice.Text);
            products.UnitsInStock = short.Parse(txtUnitsInStock.Text);
            products.UnitsOnOrder = short.Parse(txtUnitsOnOrder.Text);
            products.ReorderLevel = short.Parse(txtReorderLevel.Text);

            if (cmbDiscontinued.SelectedItem.ToString() == "False")
                products.Discontinued = false;
            else
                products.Discontinued = true;
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            llenarDatos();
            habilitar();
            habilitarBotones();
            btnGuardar.Enabled = false;
        }

        public void llenarDatos()
        {
            try
            {
                if (dgvDatos.RowCount > 0)
                {
                    txtProductID.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[0].Value.ToString();
                    txtProductName.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[1].Value.ToString();

                    string supplier = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[2].Value.ToString();
                    cmbSupplierID.SelectedItem = model.Suppliers.Where(x => x.SupplierID == Convert.ToInt32(supplier)).Select(x => x.CompanyName);

                    string category = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[3].Value.ToString();
                    cmbCategoryID.SelectedItem = model.Categories.Where(x => x.CategoryID == Convert.ToInt32(supplier)).Select(x => x.CategoryName);

                    txtQuantityPerUnit.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[4].Value.ToString();
                    txtUnitPrice.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[5].Value.ToString();
                    txtUnitsInStock.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[6].Value.ToString();
                    txtUnitsOnOrder.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[7].Value.ToString();
                    txtReorderLevel.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[8].Value.ToString();

                    string flag = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[9].Value.ToString();

                    if (flag == "False")
                        cmbDiscontinued.SelectedItem = "False";
                    else
                        cmbDiscontinued.SelectedItem = "True";
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            limpiar();
            habilitar();
            deshabilitarBotones();
            btnGuardar.Enabled = true;
            btnCancelar.Enabled = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                setear();
                if (daoProducts.insertar(products))
                    MessageBox.Show("Insertado correctamente");
                else
                    MessageBox.Show("Error al insertar");

                limpiar();
                deshabilitar();
                deshabilitarBotones();
                cargarDatos();
            }
            catch (Exception)
            {

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                setear();
                products.ProductID = Convert.ToInt32(txtProductID.Text);
                if (daoProducts.modificar(products))
                    MessageBox.Show("Modificado correctamente");
                else
                    MessageBox.Show("Error al modificar");

                limpiar();
                deshabilitar();
                deshabilitarBotones();
                cargarDatos();
            }
            catch (Exception)
            {

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (daoProducts.eliminar(Convert.ToInt32(txtProductID.Text)))
                    MessageBox.Show("Eliminado correctamente");
                else
                    MessageBox.Show("Error al eliminar");

                limpiar();
                deshabilitar();
                deshabilitarBotones();
                cargarDatos();
            }
            catch (Exception)
            {

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            deshabilitar();
            deshabilitarBotones();
        }

        private void txtProductName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
            {
                e.Handled = true;
                return;
            }
        }

        private void txtQuantityPerUnit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)) && !(e.KeyChar == '.') && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                return;
            }
            else
            {
                e.Handled = false;
                return;
            }
        }

        private void txtUnitPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)) && !(e.KeyChar == '.') && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                return;
            }
            else
            {
                e.Handled = false;
                return;
            }
        }

        private void txtUnitsInStock_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)) && !(e.KeyChar == '.') && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                return;
            }
            else
            {
                e.Handled = false;
                return;
            }
        }

        private void txtUnitsOnOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)) && !(e.KeyChar == '.') && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                return;
            }
            else
            {
                e.Handled = false;
                return;
            }
        }

        private void txtReorderLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)) && !(e.KeyChar == '.') && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                return;
            }
            else
            {
                e.Handled = false;
                return;
            }
        }
    }
}

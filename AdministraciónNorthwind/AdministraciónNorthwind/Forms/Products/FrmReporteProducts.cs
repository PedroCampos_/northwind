﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdministraciónNorthwind.Reportes;
using CrystalDecisions.Shared;

namespace AdministraciónNorthwind.Forms.Products
{
    public partial class FrmReporteProducts : Form
    {
        ParameterFields parametros = new ParameterFields();
        ParameterField miParametro = new ParameterField();
        ParameterDiscreteValue valor = new ParameterDiscreteValue();
        DAO.DaoProducts products = new DAO.DaoProducts();

        public FrmReporteProducts()
        {
            InitializeComponent(); 
            cargarCombo();
        }
        public void cargarCombo()
        {
            cmbProductName.DataSource = products.GetProducts().Select(x => x.ProductName).Distinct().ToList();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                this.miParametro.ParameterValueType = ParameterValueKind.StringParameter;
                this.miParametro.Name = "@ProductName";
                this.valor.Value = cmbProductName.Text;
                this.miParametro.CurrentValues.Add(valor);
                this.parametros.Add(miParametro);

                this.crystalReportViewer1.ParameterFieldInfo = parametros;
                RptProducts rpt = new RptProducts();
                this.crystalReportViewer1.ReportSource = rpt;
            }
            catch (Exception)
            {

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using AdministraciónNorthwind.Reportes;
using AdministraciónNorthwind.DAO;
using AdministraciónNorthwind.Model;

namespace AdministraciónNorthwind.Forms.Customers
{
    public partial class FrmReporteCustomers : Form
    {
        public FrmReporteCustomers()
        {
            InitializeComponent();
            cargarCmb();
        }

        DaoCustomers daoCustomers = new DaoCustomers();
        ParameterFields parametros = new ParameterFields();
        ParameterField miParametro = new ParameterField();
        ParameterDiscreteValue valor = new ParameterDiscreteValue();

        public void cargarCmb()
        {
            cmbCountry.DataSource = daoCustomers.customersList().Select(x => x.Country).Distinct().ToList();

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            this.miParametro.ParameterValueType = ParameterValueKind.StringParameter;
            this.miParametro.Name = "@country";
            this.valor.Value = cmbCountry.Text;
            this.miParametro.CurrentValues.Add(valor);
            this.parametros.Add(miParametro);

            this.crystalReportViewer1.ParameterFieldInfo = parametros;
            RptCustomers rpt = new RptCustomers();
            this.crystalReportViewer1.ReportSource = rpt;
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}

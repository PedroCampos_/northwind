﻿namespace AdministraciónNorthwind
{
    partial class MdiPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripOpciones = new System.Windows.Forms.MenuStrip();
            this.mantenimientoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suppliesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteCustomersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteSuppliersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteProductsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripOpciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripOpciones
            // 
            this.menuStripOpciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mantenimientoToolStripMenuItem,
            this.reportesToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.menuStripOpciones.Location = new System.Drawing.Point(0, 0);
            this.menuStripOpciones.Name = "menuStripOpciones";
            this.menuStripOpciones.Size = new System.Drawing.Size(643, 25);
            this.menuStripOpciones.TabIndex = 1;
            this.menuStripOpciones.Text = "menuStrip1";
            // 
            // mantenimientoToolStripMenuItem
            // 
            this.mantenimientoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customersToolStripMenuItem,
            this.suppliesToolStripMenuItem,
            this.productsToolStripMenuItem});
            this.mantenimientoToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mantenimientoToolStripMenuItem.Name = "mantenimientoToolStripMenuItem";
            this.mantenimientoToolStripMenuItem.Size = new System.Drawing.Size(118, 21);
            this.mantenimientoToolStripMenuItem.Text = "Mantenimiento";
            // 
            // customersToolStripMenuItem
            // 
            this.customersToolStripMenuItem.Image = global::AdministraciónNorthwind.Properties.Resources.cliente__1_;
            this.customersToolStripMenuItem.Name = "customersToolStripMenuItem";
            this.customersToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.customersToolStripMenuItem.Text = "Customers";
            this.customersToolStripMenuItem.Click += new System.EventHandler(this.customersToolStripMenuItem_Click);
            // 
            // suppliesToolStripMenuItem
            // 
            this.suppliesToolStripMenuItem.Image = global::AdministraciónNorthwind.Properties.Resources.supply;
            this.suppliesToolStripMenuItem.Name = "suppliesToolStripMenuItem";
            this.suppliesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.suppliesToolStripMenuItem.Text = "Supplies";
            this.suppliesToolStripMenuItem.Click += new System.EventHandler(this.suppliesToolStripMenuItem_Click);
            // 
            // productsToolStripMenuItem
            // 
            this.productsToolStripMenuItem.Image = global::AdministraciónNorthwind.Properties.Resources.paquete__1_;
            this.productsToolStripMenuItem.Name = "productsToolStripMenuItem";
            this.productsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.productsToolStripMenuItem.Text = "Products";
            this.productsToolStripMenuItem.Click += new System.EventHandler(this.productsToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(45, 21);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // reportesToolStripMenuItem
            // 
            this.reportesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reporteCustomersToolStripMenuItem,
            this.reporteSuppliersToolStripMenuItem,
            this.reporteProductsToolStripMenuItem});
            this.reportesToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportesToolStripMenuItem.Name = "reportesToolStripMenuItem";
            this.reportesToolStripMenuItem.Size = new System.Drawing.Size(76, 21);
            this.reportesToolStripMenuItem.Text = "Reportes";
            // 
            // reporteCustomersToolStripMenuItem
            // 
            this.reporteCustomersToolStripMenuItem.Name = "reporteCustomersToolStripMenuItem";
            this.reporteCustomersToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.reporteCustomersToolStripMenuItem.Text = "Reporte Customers";
            this.reporteCustomersToolStripMenuItem.Click += new System.EventHandler(this.reporteCustomersToolStripMenuItem_Click);
            // 
            // reporteSuppliersToolStripMenuItem
            // 
            this.reporteSuppliersToolStripMenuItem.Name = "reporteSuppliersToolStripMenuItem";
            this.reporteSuppliersToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.reporteSuppliersToolStripMenuItem.Text = "Reporte Suppliers";
            this.reporteSuppliersToolStripMenuItem.Click += new System.EventHandler(this.reporteSuppliersToolStripMenuItem_Click);
            // 
            // reporteProductsToolStripMenuItem
            // 
            this.reporteProductsToolStripMenuItem.Name = "reporteProductsToolStripMenuItem";
            this.reporteProductsToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.reporteProductsToolStripMenuItem.Text = "Reporte Products";
            this.reporteProductsToolStripMenuItem.Click += new System.EventHandler(this.reporteProductsToolStripMenuItem_Click);
            // 
            // MdiPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 424);
            this.Controls.Add(this.menuStripOpciones);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStripOpciones;
            this.Name = "MdiPrincipal";
            this.Text = "MdiPrincipal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStripOpciones.ResumeLayout(false);
            this.menuStripOpciones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripOpciones;
        private System.Windows.Forms.ToolStripMenuItem mantenimientoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suppliesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteCustomersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteSuppliersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteProductsToolStripMenuItem;
    }
}
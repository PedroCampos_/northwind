﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdministraciónNorthwind.Forms.Customers;
using AdministraciónNorthwind.Forms.Products;
using AdministraciónNorthwind.Forms.Suppliers;

namespace AdministraciónNorthwind
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //ejecuta el formulario
            //Application.Run(new FrmReporteProducts());
            Application.Run(new MdiPrincipal());
        }
    }
}

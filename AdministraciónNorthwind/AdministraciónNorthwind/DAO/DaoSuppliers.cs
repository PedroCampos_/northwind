﻿using AdministraciónNorthwind.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdministraciónNorthwind.DAO
{
    class DaoSuppliers
    {
        NorthwindEntities model = new NorthwindEntities();

        public List<Suppliers> listsuppliers()
        {
            List<Suppliers> listasp = new List<Suppliers>();
            try
            {
                listasp = model.Suppliers.ToList();
                if (listasp != null)
                    return listasp;
                else
                    throw new Exception();
            }catch(Exception ex)
            {
                return null;
            }

        }


        public bool agregar(Suppliers suppliers )
        {
            try
            {
                model.Suppliers.Add(suppliers);
                model.SaveChanges();
                return true;
            }catch(Exception ex)
            {
                return false;
            }
        }



        public bool modify (Suppliers suppliers, int codigo)
        {
            Suppliers mod = new Suppliers();
            try
            {
                mod = model.Suppliers.FirstOrDefault(x => x.SupplierID == codigo);
                mod.CompanyName = suppliers.CompanyName;
                mod.ContactName = suppliers.ContactName;
                mod.ContactTitle = suppliers.ContactTitle;
                mod.Address = suppliers.Address;
                mod.City = suppliers.City;
                mod.Region = suppliers.Region;
                mod.PostalCode = suppliers.PostalCode;
                mod.Country = suppliers.Country;
                mod.Phone = suppliers.Phone;
                mod.Fax = suppliers.Fax;
                mod.HomePage = suppliers.HomePage;

                model.SaveChanges();
                return true; 
            }catch (Exception ex)
            {
                return false;
            }
        }

        public bool eliminar(int code)
        {
            Suppliers del = new Suppliers();
            try
            {
                del = model.Suppliers.FirstOrDefault(x => x.SupplierID == code);
                model.Suppliers.Remove(del);
                model.SaveChanges();
                return true;

            }catch (Exception ex)
            {
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdministraciónNorthwind.Model;

namespace AdministraciónNorthwind.DAO
{
    class DaoCustomers
    {
        NorthwindEntities model = new NorthwindEntities();
        
        public List<Customers> customersList()
        {
            List<Customers> listCust = new List<Customers>();
            try
            {
                listCust = model.Customers.ToList();

                if (listCust != null)
                    return listCust;
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new List<Customers>();
            }
        }

        public bool insert(Customers customer)
        {
            try
            {
                model.Customers.Add(customer);
                model.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool modify(Customers customer, string id)
        {
            Customers customerModify = new Customers();
            try
            {
                customerModify = model.Customers.FirstOrDefault(x => x.CustomerID == id);

                //Asignar nuevos valores
                customerModify.CompanyName = customer.CompanyName;
                customerModify.ContactName = customer.ContactName;
                customerModify.ContactTitle = customer.ContactTitle;
                customerModify.Address = customer.Address;
                customerModify.City = customer.City;
                customerModify.Region = customer.Region;
                customerModify.PostalCode = customer.PostalCode;
                customerModify.Country = customer.Country;
                customerModify.Phone = customer.Phone;
                customerModify.Fax = customer.Fax;
                model.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool delete(string id)
        {
            Customers customerDelete = new Customers();
            try
            {
                customerDelete = model.Customers.FirstOrDefault(x => x.CustomerID == id);
                model.Customers.Remove(customerDelete);
                model.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}

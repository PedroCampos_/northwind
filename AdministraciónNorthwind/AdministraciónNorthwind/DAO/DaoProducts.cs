﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdministraciónNorthwind.Model;

namespace AdministraciónNorthwind.DAO
{
    public class DaoProducts
    {
        NorthwindEntities model = new NorthwindEntities();

        public List<Products> GetProducts()
        {
            List<Products> products = new List<Products>();
            try
            {
                products = model.Products.ToList();

                if (products != null)
                    return products;
                else
                    throw new Exception();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool insertar(Products product)
        {
            try
            {
                model.Products.Add(product);
                model.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificar(Products product)
        {
            Products modificar = new Products();
            try
            {
                modificar = model.Products.FirstOrDefault(x => x.ProductID == product.ProductID);

                modificar.ProductName = product.ProductName;
                modificar.SupplierID = product.SupplierID;
                modificar.CategoryID = product.CategoryID;
                modificar.QuantityPerUnit = product.QuantityPerUnit;
                modificar.UnitPrice = product.UnitPrice;
                modificar.UnitsInStock = product.UnitsInStock;
                modificar.UnitsOnOrder = product.UnitsOnOrder;
                modificar.ReorderLevel = product.ReorderLevel;
                modificar.Discontinued = product.Discontinued;

                model.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool eliminar(int product)
        {
            Products eliminar = new Products();
            try
            {
                eliminar = model.Products.FirstOrDefault(x => x.ProductID == product);

                model.Products.Remove(eliminar);
                model.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdministraciónNorthwind.Forms.Customers;
using AdministraciónNorthwind.Forms.Products;
using AdministraciónNorthwind.Forms.Suppliers;
using AdministraciónNorthwind.Reportes;

namespace AdministraciónNorthwind
{
    public partial class MdiPrincipal : Form
    {
        public MdiPrincipal()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void customersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCustomer customer = new FrmCustomer();
            customer.MdiParent = this;
            customer.Show();
        }

        private void suppliesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSuppliers supplier = new FrmSuppliers();
            supplier.MdiParent = this;
            supplier.Show();
        }

        private void productsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmProducts product = new FrmProducts();
            product.MdiParent = this;
            product.Show();
        }

        private void reporteCustomersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmReporteCustomers rptcustomer = new FrmReporteCustomers();
            rptcustomer.MdiParent = this;
            rptcustomer.Show();
        }

        private void reporteSuppliersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRptSuppliers rptsuppliers = new FrmRptSuppliers();
            rptsuppliers.MdiParent = this;
            rptsuppliers.Show();
        }

        private void reporteProductsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmReporteProducts rptproduct = new FrmReporteProducts();
            rptproduct.MdiParent = this;
            rptproduct.Show();
        }
    }
}
